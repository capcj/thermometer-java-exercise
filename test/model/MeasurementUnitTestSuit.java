/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.runner.RunWith;
import org.junit.runners.Suite;

/**
 *
 * @author Carlos Alexandre 
 */
@RunWith(Suite.class)
@Suite.SuiteClasses({model.NewtonTest.class, model.RankineTest.class, model.DelisleTest.class, model.KelvinTest.class, model.ReaumurTest.class, model.RomerTest.class, model.CelsiusTest.class, model.FahrenheitTest.class})
public class MeasurementUnitTestSuit {

    @BeforeClass
    public static void setUpClass() throws Exception {
        System.out.println("Measurement Unit Tests setup");
    }

    @AfterClass
    public static void tearDownClass() throws Exception {
        System.out.println("Measurement Unit Tests teardown");
    }

    @Before
    public void setUp() throws Exception {
    }

    @After
    public void tearDown() throws Exception {
    }
    
}
