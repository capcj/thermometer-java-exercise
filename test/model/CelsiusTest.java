/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Carlos Alexandre
 */
public class CelsiusTest {
    Celsius instance;
    private String convertReport;
    public CelsiusTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
        System.out.println("Celsius setup");
    }
    
    @AfterClass
    public static void tearDownClass() {
        System.out.println("Kelvin teardown");
    }
    
    @Before
    public void setUp() {
        System.out.println("Setting up Celsius instance");
        this.instance = new Celsius();
        assertNotNull(this.instance);
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of convert method, of class Celsius.
     */
    @Test
    public void testConvert() throws Exception {
        this.convertReport = "Testing Convert - %.2f%s must be %.2f%s\n";
        String originTemp = "f";
        String finalTemp = "c";
        double temp = 212.00;
        double expResult = 100.00;
        System.out.printf(this.convertReport, temp, originTemp, expResult, finalTemp);
        double result = this.instance.convert(originTemp, finalTemp, temp);
        assertEquals(expResult, result, 0.01);
        originTemp = "K";
        temp = 100.00;
        expResult = -173.15;
        System.out.printf(this.convertReport, temp, originTemp, expResult, finalTemp);
        result = this.instance.convert(originTemp, finalTemp, temp);
        assertEquals(expResult, result, 0.01);
        originTemp = "r";
        expResult = -217.59;
        System.out.printf(this.convertReport, temp, originTemp, expResult, finalTemp);
        result = this.instance.convert(originTemp, finalTemp, temp);
        assertEquals(expResult, result, 0.01);
        originTemp = "de";
        expResult = 33.33;
        System.out.printf(this.convertReport, temp, originTemp, expResult, finalTemp);
        result = this.instance.convert(originTemp, finalTemp, temp);
        assertEquals(expResult, result, 0.01);
        originTemp = "n";
        expResult = 303.03;
        System.out.printf(this.convertReport, temp, originTemp, expResult, finalTemp);
        result = this.instance.convert(originTemp, finalTemp, temp);
        assertEquals(expResult, result, 0.01);
        originTemp = "Re";
        expResult = 125.00;
        System.out.printf(this.convertReport, temp, originTemp, expResult, finalTemp);
        result = this.instance.convert(originTemp, finalTemp, temp);
        assertEquals(expResult, result, 0.01);
        originTemp = "Ro";
        expResult = 176.19;
        System.out.printf(this.convertReport, temp, originTemp, expResult, finalTemp);
        result = this.instance.convert(originTemp, finalTemp, temp);
        assertEquals(expResult, result, 0.01);
        originTemp = "c";
        expResult = 100.00;
        System.out.printf(this.convertReport, temp, originTemp, expResult, finalTemp);
        result = this.instance.convert(originTemp, finalTemp, temp);
        assertEquals(expResult, result, 0.01);
    }
    
    @Test(expected = Exception.class)
    public void testExceptionIsThrown() throws Exception {
        System.out.println("Exception checking - Sending empty values");
        this.instance.convert("", "", 0.0);
    }
    
}
