/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Carlos Alexandre
 */
public class RomerTest {
    Romer instance;
    private String convertReport;
    public RomerTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
        System.out.println("Romer setup");
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
        System.out.println("Setting up Romer instance");
        this.instance = new Romer();
        assertNotNull(this.instance);
    }
    
    @After
    public void tearDown() {
    }

    @Test
    public void testConvert() throws Exception {
        this.convertReport = "Testing Convert - %.2f%s must be %.2f%s\n";
        String originTemp = "c";
        String finalTemp = "ro";
        double temp = 100.00;
        double expResult = 60.00;
        System.out.printf(this.convertReport, temp, originTemp, expResult, finalTemp);
        double result = this.instance.convert(originTemp, finalTemp, temp);
        assertEquals(expResult, result, 0.01);
        originTemp = "K";
        expResult = -83.40;
        System.out.printf(this.convertReport, temp, originTemp, expResult, finalTemp);
        result = this.instance.convert(originTemp, finalTemp, temp);
        assertEquals(expResult, result, 0.01);
        originTemp = "de";
        expResult = 25.00;
        System.out.printf(this.convertReport, temp, originTemp, expResult, finalTemp);
        result = this.instance.convert(originTemp, finalTemp, temp);
        assertEquals(expResult, result, 0.01);
        originTemp = "f";
        expResult = 27.33;
        System.out.printf(this.convertReport, temp, originTemp, expResult, finalTemp);
        result = this.instance.convert(originTemp, finalTemp, temp);
        assertEquals(expResult, result, 0.01);
        originTemp = "n";
        expResult = 166.59;
        System.out.printf(this.convertReport, temp, originTemp, expResult, finalTemp);
        result = this.instance.convert(originTemp, finalTemp, temp);
        assertEquals(expResult, result, 0.01);
        originTemp = "r";
        expResult = -106.73;
        System.out.printf(this.convertReport, temp, originTemp, expResult, finalTemp);
        result = this.instance.convert(originTemp, finalTemp, temp);
        assertEquals(expResult, result, 0.01);
        originTemp = "Re";
        expResult = 73.12;
        System.out.printf(this.convertReport, temp, originTemp, expResult, finalTemp);
        result = this.instance.convert(originTemp, finalTemp, temp);
        assertEquals(expResult, result, 0.01);
        originTemp = "Ro";
        expResult = 100.00;
        System.out.printf(this.convertReport, temp, originTemp, expResult, finalTemp);
        result = this.instance.convert(originTemp, finalTemp, temp);
        assertEquals(expResult, result, 0.01);
    }
    
    @Test(expected = Exception.class)
    public void testExceptionIsThrown() throws Exception {
        System.out.println("Exception checking - Sending empty values");
        this.instance.convert("", "", 0.0);
    }
}
