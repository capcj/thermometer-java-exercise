/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Carlos Alexandre
 */
public class KelvinTest {
    Kelvin instance;
    private String convertReport;
    public KelvinTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
        System.out.println("Kelvin setup");
    }
    
    @AfterClass
    public static void tearDownClass() {
        System.out.println("Kelvin teardown");
    }
    
    @Before
    public void setUp() {
        System.out.println("Setting up Kelvin instance");
        this.instance = new Kelvin();
        assertNotNull(this.instance);
    }
    
    @After
    public void tearDown() {
    }
    
    /**
     * Test of convert method, of class Kelvin.
     */
    @Test
    public void testConvert() throws Exception {
        this.convertReport = "Testing Convert - %.2f%s must be %.2f%s\n";
        String originTemp = "c";
        String finalTemp = "k";
        double temp = 100.00;
        double expResult = 373.15;
        System.out.printf(this.convertReport, temp, originTemp, expResult, finalTemp);
        double result = this.instance.convert(originTemp, finalTemp, temp);
        assertEquals(expResult, result, 0.01);
        originTemp = "f";
        expResult = 310.93;
        System.out.printf(this.convertReport, temp, originTemp, expResult, finalTemp);
        result = this.instance.convert(originTemp, finalTemp, temp);
        assertEquals(expResult, result, 0.01);
        originTemp = "r";
        expResult = 55.55;
        System.out.printf(this.convertReport, temp, originTemp, expResult, finalTemp);
        result = this.instance.convert(originTemp, finalTemp, temp);
        assertEquals(expResult, result, 0.01);
        originTemp = "de";
        expResult = 306.48;
        System.out.printf(this.convertReport, temp, originTemp, expResult, finalTemp);
        result = this.instance.convert(originTemp, finalTemp, temp);
        assertEquals(expResult, result, 0.01);
        originTemp = "n";
        expResult = 576.18;
        System.out.printf(this.convertReport, temp, originTemp, expResult, finalTemp);
        result = this.instance.convert(originTemp, finalTemp, temp);
        assertEquals(expResult, result, 0.01);
        originTemp = "Re";
        expResult = 398.15;
        System.out.printf(this.convertReport, temp, originTemp, expResult, finalTemp);
        result = this.instance.convert(originTemp, finalTemp, temp);
        assertEquals(expResult, result, 0.01);
        originTemp = "Ro";
        expResult = 449.34;
        System.out.printf(this.convertReport, temp, originTemp, expResult, finalTemp);
        result = this.instance.convert(originTemp, finalTemp, temp);
        assertEquals(expResult, result, 0.01);
        originTemp = "K";
        expResult = 100.00;
        System.out.printf(this.convertReport, temp, originTemp, expResult, finalTemp);
        result = this.instance.convert(originTemp, finalTemp, temp);
        assertEquals(expResult, result, 0.01);
    }
    
    @Test(expected = Exception.class)
    public void testExceptionIsThrown() throws Exception {
        System.out.println("Exception checking - Sending empty values");
        this.instance.convert("", "", 0.0);
    }
}
