/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Carlos Alexandre
 */
public class FahrenheitTest {
    Fahrenheit instance;
    private String convertReport;
    public FahrenheitTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
        System.out.println("Fahrenheit setup");
    }
    
    @AfterClass
    public static void tearDownClass() {
        System.out.println("Fahrenheit teardown");
    }
    
    @Before
    public void setUp() {
        System.out.println("Setting up Fahrenheit instance");
        this.instance = new Fahrenheit();
        assertNotNull(this.instance);
    }
    
    @After
    public void tearDown() {
    }

    @Test
    public void testConvert() throws Exception {
        this.convertReport = "Testing Convert - %.2f%s must be %.2f%s\n";
        String originTemp = "c";
        String finalTemp = "f";
        double temp = 100.00;
        double expResult = 212.00;
        System.out.printf(this.convertReport, temp, originTemp, expResult, finalTemp);
        double result = this.instance.convert(originTemp, finalTemp, temp);
        assertEquals(expResult, result, 0.01);
        originTemp = "K";
        expResult = -279.67;
        System.out.printf(this.convertReport, temp, originTemp, expResult, finalTemp);
        result = this.instance.convert(originTemp, finalTemp, temp);
        assertEquals(expResult, result, 0.01);
        originTemp = "r";
        expResult = -359.67;
        System.out.printf(this.convertReport, temp, originTemp, expResult, finalTemp);
        result = this.instance.convert(originTemp, finalTemp, temp);
        assertEquals(expResult, result, 0.01);
        originTemp = "n";
        expResult = 577.45;
        System.out.printf(this.convertReport, temp, originTemp, expResult, finalTemp);
        result = this.instance.convert(originTemp, finalTemp, temp);
        assertEquals(expResult, result, 0.01);
        originTemp = "Re";
        expResult = 257.00;
        System.out.printf(this.convertReport, temp, originTemp, expResult, finalTemp);
        result = this.instance.convert(originTemp, finalTemp, temp);
        assertEquals(expResult, result, 0.01);
        originTemp = "Ro";
        expResult = 349.14;
        System.out.printf(this.convertReport, temp, originTemp, expResult, finalTemp);
        result = this.instance.convert(originTemp, finalTemp, temp);
        assertEquals(expResult, result, 0.01);
        originTemp = "de";
        expResult = 92.00;
        System.out.printf(this.convertReport, temp, originTemp, expResult, finalTemp);
        result = this.instance.convert(originTemp, finalTemp, temp);
        assertEquals(expResult, result, 0.01);
        originTemp = "f";
        expResult = 100.00;
        System.out.printf(this.convertReport, temp, originTemp, expResult, finalTemp);
        result = this.instance.convert(originTemp, finalTemp, temp);
        assertEquals(expResult, result, 0.01);
    }
    
    @Test(expected = Exception.class)
    public void testExceptionIsThrown() throws Exception {
        System.out.println("Exception checking - Sending empty values");
        this.instance.convert("", "", 0.0);
    }
}
