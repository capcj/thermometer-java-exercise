/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Carlos Alexandre
 */
public class DelisleTest {
    Delisle instance;
    private String convertReport;
    public DelisleTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
        System.out.println("Delisle setup");
    }
    
    @AfterClass
    public static void tearDownClass() {
        System.out.println("Delisle teardown");
    }
    
    @Before
    public void setUp() {
        System.out.println("Setting up Delisle instance");
        this.instance = new Delisle();
        assertNotNull(this.instance);
    }
    
    @After
    public void tearDown() {
    }

    @Test
    public void testConvert() throws Exception {
        this.convertReport = "Testing Convert - %.2f%s must be %.2f%s\n";
        String originTemp = "c";
        String finalTemp = "de";
        double temp = 100.00;
        double expResult = 0.00;
        System.out.printf(this.convertReport, temp, originTemp, expResult, finalTemp);
        double result = this.instance.convert(originTemp, finalTemp, temp);
        assertEquals(expResult, result, 0.01);
        originTemp = "K";
        expResult = 409.72;
        System.out.printf(this.convertReport, temp, originTemp, expResult, finalTemp);
        result = this.instance.convert(originTemp, finalTemp, temp);
        assertEquals(expResult, result, 0.01);
        originTemp = "r";
        expResult = 476.39;
        System.out.printf(this.convertReport, temp, originTemp, expResult, finalTemp);
        result = this.instance.convert(originTemp, finalTemp, temp);
        assertEquals(expResult, result, 0.01);
        originTemp = "n";
        expResult = -304.54;
        System.out.printf(this.convertReport, temp, originTemp, expResult, finalTemp);
        result = this.instance.convert(originTemp, finalTemp, temp);
        assertEquals(expResult, result, 0.01);
        originTemp = "Re";
        expResult = -37.50;
        System.out.printf(this.convertReport, temp, originTemp, expResult, finalTemp);
        result = this.instance.convert(originTemp, finalTemp, temp);
        assertEquals(expResult, result, 0.01);
        originTemp = "Ro";
        expResult = -114.28;
        System.out.printf(this.convertReport, temp, originTemp, expResult, finalTemp);
        result = this.instance.convert(originTemp, finalTemp, temp);
        assertEquals(expResult, result, 0.01);
        originTemp = "f";
        expResult = 93.33;
        System.out.printf(this.convertReport, temp, originTemp, expResult, finalTemp);
        result = this.instance.convert(originTemp, finalTemp, temp);
        assertEquals(expResult, result, 0.01);
        originTemp = "de";
        expResult = 100.00;
        System.out.printf(this.convertReport, temp, originTemp, expResult, finalTemp);
        result = this.instance.convert(originTemp, finalTemp, temp);
        assertEquals(expResult, result, 0.01);
    }
    
    @Test(expected = Exception.class)
    public void testExceptionIsThrown() throws Exception {
        System.out.println("Exception checking - Sending empty values");
        this.instance.convert("", "", 0.0);
    }
    
}
