/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

/**
 *
 * @author azoth
 */
public class Rankine extends MeasurementUnit{
    @Override
    //Convert to Kelvin
    public double convert(String originTemp, String finalTemp, double temp) throws Exception{
        try {
            super.convertInit(originTemp, finalTemp, temp);
            this.setConvertedTemp(this.convertToKelvin() * 9/5);
        } catch (Exception ex) {
            throw new Exception("Error - Couldn't convert: " + ex);
        }
        return this.getConvertedTemp();
    }
}
