/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

/**
 *
 * @author Carlos Alexandre
 */
public class LexicTranslator {
    public static String[] prepareOrder(String order) throws Exception {
        int orderSize = order.length();
        if (orderSize < 1) {
            throw new Exception("Invalid parameter, must be a String with some"
                    + "content");
        }
        String[] splitedOrder = order.split("\\s+");
        orderSize = splitedOrder.length;
        if (orderSize > 3 && (!splitedOrder[1].toLowerCase().equals("to") && 
                !splitedOrder[1].toLowerCase().equals("from"))) {
            throw new Exception("Invalid order format");
        }
        String[] translatedOrder = new String[4];
        int numericCounter = 1;
        for (int i = 0; i < orderSize; i++) {
            int splitedSize = splitedOrder[i].length();
            if (i != 1) {
                for (int j = 0; j < splitedSize; j++) {
                    if (splitedOrder[i].charAt(j) >= '0' && 
                            splitedOrder[i].charAt(j) <= '9' ||
                            splitedOrder[i].charAt(j) == '.') {
                        if (translatedOrder[i] == null) {
                            translatedOrder[i] = Character.toString(splitedOrder[i].charAt(j));
                        } else {
                            translatedOrder[i] += splitedOrder[i].charAt(j);
                        }
                    } else {
                        if (translatedOrder[i] == null) {
                            numericCounter = i;
                        } else {
                            numericCounter = i + 1;
                        }
                        translatedOrder[numericCounter] = Character.toString(splitedOrder[i].charAt(j));
                        
                    }
                }
            }
        }

        if (splitedOrder[1].toLowerCase().equals("from")) {
            String buffer = translatedOrder[0];
            translatedOrder[0] = translatedOrder[2];
            translatedOrder[1] = translatedOrder[3];
            translatedOrder[2] = buffer;
        }
        
        return translatedOrder;
    }
}
