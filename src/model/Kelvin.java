/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

/**
 *
 * @author Carlos Alexandre
 */
public class Kelvin extends MeasurementUnit {

    /**
     *
     * @param originTemp
     * @param finalTemp
     * @param temp
     * @return 
     * @throws java.lang.Exception
     */
 
    @Override
    public double convert(String originTemp, String finalTemp, double temp) throws Exception {
        try {
            super.convertInit(originTemp, finalTemp, temp);
        } catch (Exception ex) {
            throw new Exception("Error - Couldn't convert: " + ex);
        }
        switch(this.getOriginTempType()) {
            //fahrenheit
            case "f":
                this.setConvertedTemp((this.getOriginTemp() + 459.67) * 5/9);
                break;
            //kelvin
            case "k":
                this.setConvertedTemp(this.getOriginTemp());
                break;
            //rankine
            case "r":
                this.setConvertedTemp(this.getOriginTemp() * 5/9);
                break;
            //delisle
            case "de":
                this.setConvertedTemp(373.15 - (this.getOriginTemp() * 2/3));
                break;
            //newton
            case "n":
                this.setConvertedTemp((this.getOriginTemp() * 100/33) + 273.15);
                break;
            //réaumur
            case "re":
                this.setConvertedTemp((this.getOriginTemp() * 5/4) + 273.15);
                break;
            //romer
            case "ro":
                this.setConvertedTemp((this.getOriginTemp() - 7.5f) * 40/21 + 273.15);
                break;
            //celsius
            case "c":
                this.setConvertedTemp(this.getOriginTemp() + 273.15);
                break;
        }
        
        return this.getConvertedTemp();
    }
}
