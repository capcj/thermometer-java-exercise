/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

/**
 * Class made to give to the views a solid model to work with the measurement
 * units without much verbose of the main factory (which has other objective
 * too)
 *
 * @author Carlos Alexandre
 */
public class MeasurementUnitItem implements MeasurementItem {

    private final String itemDescription;
    private final String itemIndex;

    public MeasurementUnitItem(String itemIndex, String itemDescription) {
        this.itemIndex = itemIndex;
        this.itemDescription = itemDescription;
    }

    public String getItemIndex() {
        return this.itemIndex;
    }

    @Override
    public String toString() {
        return this.itemDescription;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (!MeasurementUnitItem.class.isAssignableFrom(obj.getClass())) {
            return false;
        }
        final MeasurementUnitItem other = (MeasurementUnitItem) obj;
        if ((this.itemDescription == null) ? (other.itemDescription != null) : !this.itemDescription.equals(other.itemDescription)) {
            return false;
        }
        if (this.itemIndex != other.itemIndex) {
            return false;
        }
        return true;
    }
}
