/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

/**
 *
 * @author Carlos Alexandre
 */
public class Romer extends MeasurementUnit {
    @Override
    //Convert from Kelvin
    public double convert(String originTemp, String finalTemp, double temp) throws Exception {
        try {
            super.convertInit(originTemp, finalTemp, temp);
            this.setConvertedTemp(((this.convertToKelvin()- 273.15) * 21/40) + 7.5);
        } catch (Exception ex) {
            throw new Exception("Error - Couldn't convert: " + ex);
        }
        return this.getConvertedTemp();
    }
}
