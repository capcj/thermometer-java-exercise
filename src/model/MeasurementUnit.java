/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import java.util.HashMap;
import java.util.Map;

/**
 *
 * @author Carlos Alexandre
 */
public abstract class MeasurementUnit {

    private double originTempVal;
    private double finalTempVal;
    private String originTemp;
    private String finalTemp;
    private final Map<String, String> MEASUREMENTUNITS = new HashMap<>();
    
    public MeasurementUnit() {
        MEASUREMENTUNITS.put("C", "Celsius");
        MEASUREMENTUNITS.put("F", "Fahrenheit");
        MEASUREMENTUNITS.put("K", "Kelvin");
        MEASUREMENTUNITS.put("R", "Rankine");
        MEASUREMENTUNITS.put("De", "Delisle");
        MEASUREMENTUNITS.put("N", "Newton");
        MEASUREMENTUNITS.put("Re", "Reaumur");
        MEASUREMENTUNITS.put("Ro", "Romer");
    }
    
    public Map<String, String> getMeasurementUnits() {
        return this.MEASUREMENTUNITS;
    }

    public double getOriginTemp() {
        return this.originTempVal;
    }

    public double getConvertedTemp() {
        return this.finalTempVal;
    }

    public String getOriginTempType() {
        return this.originTemp;
    }

    public String getFinalTempType() {
        return this.finalTemp;
    }
    
    protected void setConvertedTemp(double temp) {
        this.finalTempVal = temp;
    }

    protected double convertToKelvin() throws Exception {
        Kelvin kelvin = null;
        try {
            kelvin = new Kelvin();
        } catch (Exception ex) {
            throw new Exception("Couldn't load Kelvin converter: " + ex);
        }
        return kelvin.convert(originTemp, "k", originTempVal);
    }

    protected void convertInit(String originTemp, String finalTemp, double temp) throws Exception {
        if (originTemp.equals("") || finalTemp.equals("")) {
            throw new IllegalArgumentException(
                    "Invalid Temperatures Types, insert a valid one:"
                    + " - C (Celsius);\n"
                    + " - F (Fahrenheit);\n"
                    + " - K (Kelvin);\n"
                    + " - R (Rankine);\n"
                    + " - De (Delisle);\n"
                    + " - N (Newton);\n"
                    + " - Re (Réaumur);\n"
                    + " - Ro (Romer);\n"
            );
        }
        if (temp == 0.0) {
            throw new IllegalArgumentException("Invalid temperature, insert a value above 0");
        }
        this.originTemp = originTemp.toLowerCase();
        this.finalTemp = finalTemp.toLowerCase();
        this.originTempVal = temp;
    }

    public abstract double convert(String originTemp, String finalTemp, double temp) throws Exception;
}
