/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

/**
 *
 * @author Carlos Alexandre
 */
public interface MeasurementItem {
    public String getItemIndex();
    public String toString();
    public boolean equals(Object obj);
}
