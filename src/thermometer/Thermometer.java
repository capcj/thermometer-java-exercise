/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package thermometer;
import java.util.Scanner;
/**
 *
 * @author Carlos Alexandre
 */
public class Thermometer {
    public static void main(String[] args) throws Exception {
        Scanner input = new Scanner(System.in);
        int mode = 4;
        char sureExit;
        while (mode != 0) {
            System.out.println();
            System.out.println("------------------------------------");
            System.out.println("-------------Thermometer------------");
            System.out.println("------------------------------------");
            System.out.println("What do you want?\n 1) GUI 2) Simple mode"
                    + " 3) Human Mode 0) Exit");
            mode = input.nextInt();
            switch(mode) {
                case 1:
                    GuiMode.main(args);
                    break;
                case 2:
                    SimpleMode.main(args);
                    break;
                case 3:
                    HumanMode.main(args);
                    break;
                case 0:
                    System.out.print("Are you sure? y/n ");
                    sureExit = input.next().charAt(0);
                    if (Character.toLowerCase(sureExit) == 'n') {
                        mode = 4;
                    } else {
                        System.out.print("\nGoodbye :)");
                    }
                    break;
            }
        }
    }
}
