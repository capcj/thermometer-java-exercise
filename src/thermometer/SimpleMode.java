/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package thermometer;

import controller.ThermometerFactory;
import java.util.Map;
import java.util.Scanner;
import model.MeasurementUnit;

/**
 *
 * @author Carlos Alexandre
 */
public class SimpleMode {
    public static void main(String[] args) throws Exception {
        Scanner input = new Scanner(System.in);
        double temperature;
        String initialTemp;
        String finalTemp;
        MeasurementUnit thermometer;
        Map<String, String> measurementUnitsAccepted = ThermometerFactory.getMeasurementUnits();
        StringBuffer strbuf = new StringBuffer();
        measurementUnitsAccepted.forEach((k,v)->strbuf.append("\n")
                                                        .append(k)
                                                        .append(": ")
                                                        .append(v)
                                        );
        String measurementUnitsAcceptedText = strbuf.toString();
        System.out.println("Insert the temperature you want to convert: ");
        temperature = input.nextDouble();
        System.out.println(measurementUnitsAcceptedText);
        System.out.println("Select the original measurement unit: ");
        initialTemp = input.next();
        System.out.println(measurementUnitsAcceptedText);
        System.out.println("Select the final measurement unit:  ");
        finalTemp = input.next();
        thermometer = ThermometerFactory.mountUnit(initialTemp, finalTemp, temperature);
        System.out.printf("\n%s\t%.2f\n", "The temperature you typed was", thermometer.getConvertedTemp());
    }
}
