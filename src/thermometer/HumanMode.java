/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package thermometer;

import controller.ThermometerFactory;
import java.util.Scanner;
import model.LexicTranslator;
import model.MeasurementUnit;

/**
 *
 * @author Carlos Alexandre
 */
public class HumanMode {
    public static void main(String[] args) throws Exception {
        Scanner input = new Scanner(System.in);
        String conversionOrder;
        MeasurementUnit thermometer;
        System.out.print("\nInsert your conversion (Ex: 100C to F): ");
        conversionOrder = input.nextLine();
        String[] order = LexicTranslator.prepareOrder(conversionOrder);
        double orderTemp = Double.parseDouble(order[0]);
        thermometer = ThermometerFactory.mountUnit(order[1], order[2], orderTemp);
        System.out.printf("%s\t%.2f%s\n", 
                "The temperature converted is", 
                thermometer.getConvertedTemp(), 
                order[2]
        );
    }
}
