/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import java.util.Map;
import model.Celsius;
import model.Delisle;
import model.Fahrenheit;
import model.Kelvin;
import model.MeasurementUnit;
import model.Newton;
import model.Rankine;
import model.Reaumur;
import model.Romer;

/**
 *
 * @author Carlos Alexandre
 */
public class ThermometerFactory {
    public static MeasurementUnit mountUnit(String originTemp, String finalTemp, double temp) throws Exception {
        switch(finalTemp.toLowerCase()) {
            case "c":
                Celsius celsius = new Celsius();
                celsius.convert(originTemp, finalTemp, temp);
                return celsius;
            case "f":
                Fahrenheit fahrenheit = new Fahrenheit();
                fahrenheit.convert(originTemp, finalTemp, temp);
                return fahrenheit;
            case "k":
                Kelvin kelvin = new Kelvin();
                kelvin.convert(originTemp, finalTemp, temp);
                return kelvin;
            case "r":
                Rankine rankine = new Rankine();
                rankine.convert(originTemp, finalTemp, temp);
                return rankine;
            case "de":
                Delisle delisle = new Delisle();
                delisle.convert(originTemp, finalTemp, temp);
                return delisle;
            case "n":
                Newton newton = new Newton();
                newton.convert(originTemp, finalTemp, temp);
                return newton;
            case "re":
                Reaumur reaumur = new Reaumur();
                reaumur.convert(originTemp, finalTemp, temp);
                return reaumur;
            case "ro":
                Romer romer = new Romer();
                romer.convert(originTemp, finalTemp, temp);
                return romer;
            default:
                return null;
        }
    }
    
    public static Map<String, String> getMeasurementUnits() throws Exception {
        Kelvin kelvin = new Kelvin();
        return kelvin.getMeasurementUnits();
    }
}

